/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class FirstTaskView extends CancelableState {
        public static Name: string = 'FirstTaskView';

        public name: string = FirstTaskView.Name;

        private totalCount: number = 144;
        private stack1: Stack;
        private stack2: Stack;
        private loopEvent: Phaser.TimerEvent;

        constructor() {
            super();
        }

        public create(): void {
            super.create();

            this.createStacks();
            this.createInterval();
        }

        private createInterval(): void {
            this.loopEvent = this.game.time.events.loop(1000, this.moveCardBetweenStacks, this);
        }

        private moveCardBetweenStacks(): void {
            if (!this.stack1.isEmpty()) {
                this.stack1.moveTopCardToStack(this.stack2);
            } else {
                this.game.time.events.remove(this.loopEvent);
                this.loopEvent = null;
            }
        }

        private createStacks(): void {
            this.stack1 = new Stack(this.game);
            this.stack1.y = 200;
            this.stack2 = new Stack(this.game);
            this.stack2.y = 300;

            for (let i: number = 0; i < this.totalCount; i++) {
                let card: Phaser.Image = this.game.make.image(0, 0, 'atlas', 'card' + this.getRandomCard().toString());
                this.stack1.addCard(card);
            }
        }

        private getRandomCard(): number {
            return Math.floor(Math.random() * 3);
        }
        public shutdown(): void {
            if (this.loopEvent) {
                this.game.time.events.remove(this.loopEvent);
                this.loopEvent = null;
            }
            this.stack1.destroy(true);
            this.stack2.destroy(true);
            this.stack1 = null;
            this.stack2 = null;
            super.shutdown();
        }
    }
}
