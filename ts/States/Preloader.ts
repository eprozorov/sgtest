/**
 * Created by evgeny on 21/08/2017.
 */
module SoftGamesTest {
    export class Preloader extends Phaser.State {
        public static Name: string = 'Preloader';

        public name: string = Preloader.Name;

        constructor() {
            super();
        }
        public preload(): void {
            this.game.load.atlas('atlas', 'assets/images/font.png', 'assets/images/atlas.json');
            this.game.load.xml('font', 'assets/images/font.fnt');
        }
        public create(): void {
            this.cache.addBitmapFont('MyFont', null, this.cache.getImage('atlas'), this.cache.getXML('font'), 'xml');
            this.game.state.start(MenuView.Name);
        }
    }
}
