/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class ThirdTaskView extends CancelableState {
        public static Name: string = 'ThirdTaskView';
        public name: string = ThirdTaskView.Name;
        private emitter: Phaser.Particles.Arcade.Emitter;

        constructor() {
            super();
        }

        public create(): void {
            super.create();
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.createEmitter();
        }

        private createEmitter(): void {
            this.emitter = this.game.add.emitter(this.game.world.centerX, this.game.world.centerY, 10);
            this.emitter.gravity = 0;
            this.emitter.setAlpha(1, 0.5);
            this.emitter.minParticleScale = -0.8;
            this.emitter.maxParticleScale = 0.8;
            this.emitter.minRotation = 0;
            this.emitter.maxRotation = Math.PI;
            this.emitter.setYSpeed(-150, -550);
            this.emitter.makeParticles(['atlas'], ['particle1']);
            this.emitter.forEach(function(particle: Phaser.Particle): any {
                particle.tint = 0xff622c;
            }, null);
            this.emitter.start(false, 200, 0.02, -1);
        }

        public shutdown(): void {
            this.emitter.destroy(true);
            this.emitter = null;
            super.shutdown();
        }
    }
}
