/**
 * Created by evgeny on 21/08/2017.
 */
module SoftGamesTest {
    export class MenuView extends Phaser.State {
        public static Name: string = 'MenuView';

        public name: string = MenuView.Name;

        constructor() {
            super();
        }

        public create(): void {
            super.create();

            this.game.add.button(100, 200, 'atlas', () => this.startView(0), null, 'card0', 'card0', 'card0', 'card0');
            this.game.add.button(200, 200, 'atlas', () => this.startView(1), null, 'card1', 'card1', 'card1', 'card1');
            this.game.add.button(300, 200, 'atlas', () => this.startView(2), null, 'card2', 'card2', 'card2', 'card2');
        }

        private startView(task: number): void {
            switch (task) {
                case 0:
                    this.game.state.start(FirstTaskView.Name);
                    break;
                case 1:
                    this.game.state.start(SecondTaskView.Name);
                    break;
                case 2:
                    this.game.state.start(ThirdTaskView.Name);
                    break;
                default:
                    break;
            }
        }
    }
}
