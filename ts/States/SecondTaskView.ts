/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class SecondTaskView extends CancelableState {
        public static Name: string = 'SecondTaskView';
        public name: string = SecondTaskView.Name;

        private emojiText: EmojiText;
        private loopEvent: Phaser.TimerEvent;
        private wordsString: string = 'With us any portal owner carrier or app developer can easily and effectively ' +
            'engage and monetize their user base with free HTML5 games either as stand alone game feed or full white ' +
            'label version We work with thousands of partners including leading multi-national companies such as Microsoft ' +
            'Amazon and Mozilla You want to add 350+ free HTML5 games to your site or app You dont have a mobile optimized ' +
            'site yet Let us help you to generate constant growing advertising and gaming revenue streams Get in touch with us ' +
            'and learn how SOFTGAMES can help you';
        private words: string[];
        constructor() {
            super();
            this.words = this.wordsString.split(' ');
        }

        public create(): void {
            super.create();

            this.createEmojiText();
            this.createInterval();
        }

        private createInterval(): void {
            this.loopEvent = this.game.time.events.loop(5000, this.changeEmojiText, this);
        }

        private changeEmojiText(): void {
            let resultString: string = '';
            let wordsCount: number = Math.floor(Math.random() * (this.words.length - 10)) + 10;
            for (let i: number = 0; i < wordsCount; i++) {
                resultString += this.words[Math.floor(Math.random() * this.words.length)] + ' ';
                if (Math.random() < 0.3) {
                    resultString += '%e ';
                }
            }
            this.emojiText.setTextAndSize(resultString, Math.floor(Math.random() * 10) + 20);
        }
        private createEmojiText(): void {
            this.emojiText = new EmojiText(this.game, this.game.add.group());
            this.emojiText.y = 100;
            this.changeEmojiText();
        }
        public shutdown(): void {
            if (this.loopEvent) {
                this.game.time.events.remove(this.loopEvent);
                this.loopEvent = null;
            }
            this.emojiText.destroy(true);
            this.emojiText = null;
            super.shutdown();
        }
    }
}
