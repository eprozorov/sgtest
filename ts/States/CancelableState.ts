/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class CancelableState extends Phaser.State {

        constructor() {
            super();
        }

        public create(): void {
            this.game.time.advancedTiming = true;
            this.game.input.onDown.add(this.onTouch, this);
            super.create();
        }

        public render (): void {
            this.game.debug.text(this.game.time.fps.toString(), 2, 14, '#00ff00');
        }

        private onTouch (): void {
            this.game.state.start(MenuView.Name);
        }
    }
}
