module SoftGamesTest {
    export class Boot extends Phaser.State {
        public static Name: string = 'boot';

        public name: string = Boot.Name;

        constructor() {
            super();
        }

        /**
         * Init, this is where game and google analytics are set up.
         * Small tweaks such as limiting input pointers, disabling right click context menu are placed here
         */
        public init(): void {
            //input pointers limited to 1
            this.game.input.maxPointers = 1;
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.game.scale.windowConstraints.bottom = 'visual';

            this.game.onBlur.add((data: any) => {
                this.game.sound.mute = true;
            });
            this.game.onFocus.add((data: any) => {
                this.game.sound.mute = false;
            });
        }

        /**
         * Load all the assets needed for the preloader before starting the game.
         * First load cachebuster before running Splash screen preloader.
         * The preloader will load all the assets while displaying portal specific splash screen.
         */
        public create(): void {
            this.game.state.start(Preloader.Name);
        }
    }
}
