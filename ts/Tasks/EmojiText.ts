/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class EmojiText extends Phaser.SpriteBatch {
        private text: string;
        private fontSize: number;

        constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer) {
            super (game, parent);
        }

        public setTextAndSize(txt: string, fontSize: number): void {
            this.removeAll();

            this.text = txt;
            this.fontSize = fontSize;
            this.createText();
        }

        private createText(): void {
            let existInTheEnd: boolean = this.text.lastIndexOf('%e') === this.text.length - 2;
            let existInTheStart: boolean = this.text.indexOf('%e') === 0;
            let arr: string[] = this.text.split('%e');
            let x0: number = 0;
            let y0: number = 0;
            let emoji: Phaser.Sprite;
            if (existInTheStart) {
                this.addEmoji(x0, y0);
                emoji = this.addEmoji(x0, y0);
                x0 = emoji.x + emoji.width;
                arr[0] = ' ' + arr[0];
            }
            for (let i: number = 0; i < arr.length; i++) {
                let txt: Phaser.BitmapText = this.game.add.bitmapText(x0, y0, 'MyFont', arr[i] + ' ', this.fontSize, this);
                if (x0 + txt.width > this.game.width) {
                    txt.x = 0;
                    y0 += txt.height + txt.height / 2;
                    txt.y = y0;
                }
                x0 = txt.x + txt.width;
                if (i !== arr.length - 1 || i === arr.length - 1 && existInTheEnd) {
                    emoji = this.addEmoji(x0, y0);
                    if (x0 + emoji.width > this.game.width) {
                        emoji.x = 0;
                        y0 += txt.height + txt.height / 2;
                        emoji.y = y0;
                    }
                    x0 = emoji.x + emoji.width;
                }
            }

        }

        private addEmoji(x0: number, y0: number): Phaser.Sprite {
            let e: Phaser.Sprite = this.game.add.sprite(x0, y0, 'atlas', 'smile', this);
            e.height = this.fontSize;
            e.scale.x = e.scale.y;
            return e;
        }
    }
}
