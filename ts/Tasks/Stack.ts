/**
 * Created by evgeny on 29/08/2017.
 */
module SoftGamesTest {
    export class Stack extends Phaser.Group {
        public lastX: number;
        private deltaX: number = 7;
        private animationTime: number = 2000;
        constructor(game: Phaser.Game) {
            super(game);
            this.lastX = 0;
        }

        public addCard(card: Phaser.Image, animate: boolean = false): void {
            if (!animate) {
                card.x = this.lastX;
            } else {
                let pos: PIXI.Point = this.toLocal(card.position, card.parent);
                card.x = pos.x;
                card.y = pos.y;
                let tween: Phaser.Tween = this.game.add.tween(card);
                tween.to({x: this.lastX, y: 0}, this.animationTime, Phaser.Easing.Cubic.Out, true);
            }
            this.lastX += this.deltaX;
            this.add(card);
        }

        public isEmpty(): boolean {
            return this.children.length === 0;
        }

        public moveTopCardToStack(stack: Stack): void {
            stack.addCard(this.getTop(), true);
            this.lastX -= this.deltaX;
        }
    }
}
