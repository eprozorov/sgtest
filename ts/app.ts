//Module name should equal the game name
module SoftGamesTest {
    export class Game extends Phaser.Game {
        constructor() {
            //We use Phaser's config object to create the game, since this is the only way to disable debugging
            //noinspection TypeScriptValidateTypes
            super({
                enableDebug: true,
                width: 700,
                height: 450,
                renderer: Phaser.AUTO,
                parent: 'content',
                transparent: true,
                antialias: true,
                preserveDrawingBuffer: false,
                physicsConfig: null,
                seed: '',
                state: null,
                forceSetTimeOut: false
            });

            this.clearBeforeRender = true;

            //Here we adjust some stuff to the game that we need, before any state is being run
            Phaser.Device.whenReady(() => {
                //Fix for mobile portals and IE
                this.stage.disableVisibilityChange = true; //This will make sure the game runs out-of-focus
                let event: string = this.device.desktop ? 'click' : 'touchstart';
                document.getElementById('content').addEventListener(event, (e: Event) => {
                    //This will make sure the game will rerun, when focus was lost
                    this.gameResumed(e);
                });
            });

            this.state.add('game', {create: this.stateCreator.bind(this), preload: this.statePreloader.bind(this)}, true);
        }

        /**
         * Here we load all the orange games scripts we need
         */
        private statePreloader(): void {
            libs.forEach((library: string) => {
                this.load.script(library, library);
            });
        }

        private stateCreator(): void {
            //Here we load all the plugins

            //Here we load all the states, but they shouldn't start automatically
            this.state.add(Boot.Name, Boot, false);
            this.state.add(Preloader.Name, Preloader, false);
            this.state.add(MenuView.Name, MenuView, false);
            this.state.add(FirstTaskView.Name, FirstTaskView, false);
            this.state.add(SecondTaskView.Name, SecondTaskView, false);
            this.state.add(ThirdTaskView.Name, ThirdTaskView, false);

            //start the game
            this.state.start(Boot.Name);
            this.state.remove('game');
        }
    }
}
